<?php

namespace App\Repositories;

use Carbon\Carbon;

   /* Memory store:
    *  $array => [
    *      'shez' => [
    *          'following' => [
    *              'jon', 'bob',
    *          ]
    *          tweets => [
    *              'timestamp' => 'comment',
    *              'timestamp  => 'comment',
    *          ],
    *       ]
    *  ]
    *
    */

class StorageRepository {

    public $data = [];

    public function __construct()
    {
        $this->data = [];
    }

    public function addMessage($user, $message)
    {
        $this->data[$user]['tweets'][time()] = $message;

        return true;
    }

    public function getMessages($user, $appendUsername=false)
    {
        $tweets = $this->data[$user]['tweets'];

        foreach ($tweets as $time => $tweet) {
            $msg = $appendUsername ? $user . ": " : '';
            $msg .= $tweet
                . " " . Carbon::createFromTimestamp($time)->diffForHumans();

            $tweets[$time] = $msg;
        }

        krsort($tweets);

        return $tweets;
    }

    public function userFollowsAnother($user1, $user2)
    {
        $this->data[$user1]['following'][] = $user2;

        return true;
    }

    public function getUsersFollowedBy($user)
    {
        sort($this->data[$user]['following']);

        return $this->data[$user]['following'];
    }

    public function getWallFor($user)
    {
        $users = $this->getUsersFollowedBy($user);
        $users[] = $user;

        $comments = [];

        foreach ($users as $user)
        {
            $comments = $comments + $this->getMessages($user, true);
        }

        krsort($comments);

        return $comments;
    }
}
