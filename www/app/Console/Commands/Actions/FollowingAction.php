<?php

namespace App\Console\Commands\Actions;

use App\Repositories\StorageRepository;

class FollowingAction implements ActionsInterface {

    public $storageRepository;
    public $user1;
    public $user2;

    public function __construct(StorageRepository $storageRepository,
                                string $user1,
                                string $user2)
    {
        $this->storageRepository = $storageRepository;
        $this->user1 = $user1;
        $this->user2 = $user2;
    }

    public function getLabel()
    {
        return "Follow";
    }

    public function handle()
    {
        $this->storageRepository->userFollowsAnother($this->user1, $this->user2);

        return 'User successfully followed!';
    }
}
