<?php

namespace App\Console\Commands\Actions;

use App\Repositories\StorageRepository;

class WallAction implements ActionsInterface {

    public $storageRepository;
    public $user;

    public function __construct(StorageRepository $storageRepository,
                                string $user)
    {
        $this->storageRepository = $storageRepository;
        $this->user = $user;
    }

    public function getLabel()
    {
        return "Wall";
    }


    public function handle()
    {
        return implode(
            "\n",
            $this->storageRepository->getWallFor($this->user)
        );
    }
}
