<?php

namespace App\Console\Commands\Actions;

use App\Repositories\StorageRepository;

class ReadingAction implements ActionsInterface {


    public $storageRepository;
    public $user;

    public function __construct(StorageRepository $storageRepository,
                                string $user)
    {
        $this->storageRepository = $storageRepository;
        $this->user = $user;
    }

    public function getLabel()
    {
        return "Reading";
    }


    public function handle()
    {
        return implode(
            "\n",
            $this->storageRepository->getMessages($this->user, false)
        );
    }
}
