<?php

namespace App\Console\Commands\Actions;

use App\Repositories\StorageRepository;

class PostingAction implements ActionsInterface {

    public $storageRepository;
    public $message;
    public $user;

    public function __construct(StorageRepository $storageRepository,
                                string $user,
                                string $message)
    {
        $this->storageRepository = $storageRepository;
        $this->user = $user;
        $this->message = $message;
    }

    public function getLabel()
    {
        return "Posting";
    }


    public function handle()
    {
        $this->storageRepository->addMessage($this->user, $this->message);

        return 'Posting Added to the user\'s wall';
    }

}
