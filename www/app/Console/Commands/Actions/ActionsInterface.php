<?php

namespace App\Console\Commands\Actions;

interface ActionsInterface {

    public function getLabel();
    public function handle();

}
