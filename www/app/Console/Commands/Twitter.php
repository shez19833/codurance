<?php

namespace App\Console\Commands;

use App\Console\Commands\Actions\FollowingAction;
use App\Console\Commands\Actions\PostingAction;
use App\Console\Commands\Actions\ReadingAction;
use App\Console\Commands\Actions\WallAction;
use App\Repositories\StorageRepository;
use Illuminate\Console\Command;

class Twitter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sys:twitter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Twitter Replica';

    public $storageRepository;

    public function __construct(StorageRepository $storageRepository)
    {
        parent::__construct();

        $this->storageRepository = $storageRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $continue = true;
        while ($continue) {
            $this->table(['Options'],
                [
                    ['posting: <user name> -> <message>'],
                    ['reading: <user name>'],
                    ['following: <user name> follows <another user>'],
                    ['wall: <user name> wall'],
                    ['exit: exit'],
                ]
            );

            $answer = $this->ask('What would you like to do');

            if ($answer == 'exit') {
                break;
            }

            $action = $this->getAction($answer);

            $this->info($action->getLabel());

            $this->info($action->handle());
        }
    }


    private function getAction($answer)
    {
        $arguments = explode(" ", $answer);

        switch (count($arguments)) {

            case 1:
                return new ReadingAction($this->storageRepository, $arguments[0]);
                break;

            case 2:
                return new WallAction($this->storageRepository, $arguments[0]);
                break;

            default:
                return $arguments[1] == 'follows'
                    ? new FollowingAction($this->storageRepository, $arguments[0], $arguments[2])
                    : new PostingAction($this->storageRepository, $arguments[0], explode(" -> ", $answer)[1]);
        }
    }
}
