<?php

namespace Tests\Unit;

use App\Repositories\StorageRepository;
use Carbon\Carbon;
use Tests\TestCase;

class TwitterTest extends TestCase
{

    public function test_user_can_post_a_message()
    {
        $this->artisan('sys:twitter')
            ->expectsQuestion('What would you like to do', 'shez -> msg1')
            ->expectsOutput('Posting Added to the user\'s wall')
            ->expectsQuestion('What would you like to do', 'exit');
    }

    public function test_user_can_see_posted_messages()
    {
        $this->artisan('sys:twitter')
            ->expectsQuestion('What would you like to do', 'shez -> msg1')
            ->expectsQuestion('What would you like to do', 'shez')
            ->expectsOutput('msg1 1 second ago')
            ->expectsQuestion('What would you like to do', 'exit');
    }

    public function test_user_can_follow_other_users()
    {
        $this->artisan('sys:twitter')
            ->expectsQuestion('What would you like to do', 'shez -> msg1')
            ->expectsQuestion('What would you like to do', 'shez1 -> msg2')
            ->expectsQuestion('What would you like to do', 'shez follows shez1')
            ->expectsOutput('User successfully followed!')
            ->expectsQuestion('What would you like to do', 'exit');
    }

    public function test_user_can_see_their_wall()
    {
        $this->artisan('sys:twitter')
            ->expectsQuestion('What would you like to do', 'shez -> msg1')
            ->expectsQuestion('What would you like to do', 'shez1 -> msg2')
            ->expectsQuestion('What would you like to do', 'shez follows shez1')
            ->expectsQuestion('What would you like to do', 'shez wall')
            ->expectsOutput("Wall")
            ->expectsQuestion('What would you like to do', 'exit');
    }
}
