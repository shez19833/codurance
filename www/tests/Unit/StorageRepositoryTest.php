<?php

namespace Tests\Unit;

use App\Repositories\StorageRepository;
use Carbon\Carbon;
use Tests\TestCase;

class StorageRepositoryTest extends TestCase
{
    public function test_user_can_store_user_with_message()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez', 'wahey wahey!');

        $result = $storageRepository->getMessages('shez');

        $this->assertStringStartsWith('wahey wahey!', array_shift($result));
    }

    public function test_user_can_store_multiple_messages()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez', 'wahey!');
        sleep(1);
        $storageRepository->addMessage('shez', 'crikey!');

        $result = $storageRepository->getMessages('shez');

        $this->assertStringStartsWith('crikey!', array_shift($result));
        $this->assertStringStartsWith('wahey!', array_shift($result));
    }

    public function test_storing_messages_have_a_timestamp()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez', 'wahey!');

        $result = array_keys($storageRepository->getMessages('shez'));

        $key = array_shift($result);

        $this->assertTrue(Carbon::createFromTimestamp($key)->isCurrentDay());
    }

    public function test_messages_are_displayed_in_descending_order()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez', 'wahey!');
        sleep(2);
        $storageRepository->addMessage('shez', 'crikey!');

        $result = $storageRepository->getMessages('shez');

        $this->assertStringStartsWith('crikey', array_shift($result));
        $this->assertStringStartsWith('wahey', array_shift($result));
    }

    public function test_user_can_follow_another_user()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez1', 'wahey!');
        $storageRepository->addMessage('shez2', 'wahey2!');

        $storageRepository->userFollowsAnother('shez1', 'shez2');

        $users = $storageRepository->getUsersFollowedBy('shez1');

        $this->assertEquals('shez2', array_shift($users));
    }

    public function test_user_can_see_comments_by_people_followed_on_their_wall()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez1', 'wahey!');
        sleep(1);
        $storageRepository->addMessage('shez2', 'wahey2!');

        $storageRepository->userFollowsAnother('shez1', 'shez2');

        $comments = $storageRepository->getWallFor('shez1');

        $this->assertStringContainsString('wahey2', array_shift($comments));
        $this->assertStringContainsString('wahey', array_shift($comments));
    }

    public function test_users_wall_have_comments_in_descending_order_by_timestamp()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez1', 'wahey!');
        sleep(1);
        $storageRepository->addMessage('shez2', 'wahey2!');
        sleep(1);
        $storageRepository->addMessage('shez3', 'wahey3!');
        sleep(1);

        $storageRepository->userFollowsAnother('shez1', 'shez2');
        $storageRepository->userFollowsAnother('shez1', 'shez3');

        $comments = $storageRepository->getWallFor('shez1');

        $this->assertStringContainsString('wahey3', array_shift($comments));
        $this->assertStringContainsString('wahey2', array_shift($comments));
        $this->assertStringContainsString('wahey', array_shift($comments));
    }

    public function test_users_wall_comments_have_user_name()
    {
        $storageRepository = new StorageRepository();

        $storageRepository->addMessage('shez1', 'wahey!');
        sleep(1);
        $storageRepository->addMessage('shez2', 'wahey2!');
        sleep(1);
        $storageRepository->addMessage('shez3', 'wahey3!');
        sleep(1);

        $storageRepository->userFollowsAnother('shez1', 'shez2');
        $storageRepository->userFollowsAnother('shez1', 'shez3');

        $comments = $storageRepository->getWallFor('shez1');

        $this->assertStringStartsWith('shez3', array_shift($comments));
        $this->assertStringStartsWith('shez2', array_shift($comments));
        $this->assertStringStartsWith('shez1', array_shift($comments));
    }
}
