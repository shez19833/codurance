# Twitter Clone

This is a console app where you can do basic stuff like:

* Post a message
* See your messages
* Follow another user
* See your wall with messages (incl those you follow)


# Pre-Running the App

There are two ways to get started - either install php or using docker if you have docker

## Standalone

### Installing PHP

for mac if you have brew you can simply run the command ```brew install php``` to install php.

### Docker

* Install docker (and for mac: docker for mac; https://docs.docker.com/docker-for-mac/install/)
* go to root folder (just above www)
* run ```make build-containers```
* run ```make up```
* run ```make build```
* run ```make ssh```


## Running the App

For stand alone make sure you are inside www/ & 
for docker make sure you are one directory above www and you have ssh'd into it.

run ```php artisan sys:twitter``` to get started.

## Running Tests

run ```vendor/phpunit/phpunit/phpunit``` to run tests

# Some images if you cannot get the app running for some reason


![Alt text](images/s.png?raw=true)
![Alt text](images/e.png?raw=true)
![Alt text](images/c.png?raw=true)
![Alt text](images/o.png?raw=true)

# Test 

![Alt text](images/test.png?raw=true)